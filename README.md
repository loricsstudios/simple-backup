# Simple backup

Barebones backup automation script that zips up target folders recursively applying include and excludes, combines those zippeds along with a metadata file into another zip which it then saves in a predefined location.

(optionally apply encryption or password protection on those files so that they are stored in the cloud encrypted)

Naming of the files is created in a way that allows this location to be f.e. a *Google drive* or *OneDrive* folder so that this data is always synced up to the cloud.